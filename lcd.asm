;http://mahler.cse.unsw.edu.au/webcms2/course/showfile.php?cid=2271&color=colour8&addr=Tutorials/MonorailEmulator.pdf

.include "m64def.inc"

.def blink_status	= r12
.def train_status 	= r13
.def move_cursor 	= r14
.def num_stat	= r15				; number of station
.def temp 		= r16
.def temp2 		= r17
.def temp3		= r18
.def row 		= r19
.def col 		= r20
.def release	= r21
.def count		= r22
.def count2		= r23
.def del_lo 	= r24
.def del_hi 	= r25
.equ str_len	= 16				; general string store in flash meory have 
									; lenght of 16 chars
;PWM set up
.equ RUN		= 153 				; at 60% duty cycle
.equ STOP		= 0
.equ FAST_PWM	= 0b01101010		; set fast PWM, invert and prescale of 8

;Keypad set up
.equ PORTCDIR 	= 0xF0
.equ INITCOLMASK= 0xEF
.equ INITROWMASK= 0x01
.equ ROWMASK 	= 0x0F
.equ HASHCOL	= 0xBF
.equ HASHROW	= 0x08

;LCD protocol control bits
.equ LCD_RS = 3
.equ LCD_RW = 1
.equ LCD_E 	= 2

;LCD functions
.equ LCD_FUNC_SET = 0b00110000
.equ LCD_DISP_OFF = 0b00001000
.equ LCD_DISP_CLR = 0b00000001
.equ LCD_DISP_ON  = 0b00001100
.equ LCD_ENTRY_SET= 0b00000100
.equ LCD_ADDR_SET = 0b10000000
.equ LCD_CURS_LEF = 0b00010000
.equ LCD_CURS_RIG = 0b00010100
;LCD function bits and constants
.equ LCD_BF = 7
.equ LCD_N 	= 3
.equ LCD_F 	= 2
.equ LCD_ID = 1
.equ LCD_S 	= 0
.equ LCD_C 	= 1
.equ LCD_B 	= 0
.equ LCD_LINE1 = 0
.equ LCD_LINE2 = 0x40

;LED set up
.equ LED_ON		= 0b00000011
.equ LED_OF		= 0b00000000

.MACRO Clear 			; This macro initializes a two byte var to 0 
ldi r28, low(@0) 		; Load the low byte of label @0 
ldi r29, high(@0) 		; Load the high byte of label @0 
clr temp 
st y+, temp 
st y, temp 				; Initialize the two-byte integer at @0 to 0 
.ENDMACRO

.dseg 
station_names: 	.byte 110
time_travel:	.byte 10
stop_time:		.byte 1
TempCounter: 	.byte 2
SecondCounter: 	.byte 2
NextEventSecond:.byte 2
BlinkCounter:	.byte 2

.cseg
.org 0
jmp RESET 
jmp DEFAULT 								; No handling for IRQ0 
jmp DEFAULT 								; No handling for IRQ1
jmp DEFAULT 								; No handling for IRQ2
jmp DEFAULT 								; No handling for IRQ3
jmp EXT_INT4_5 								; No handling for IRQ4
jmp EXT_INT4_5 								; No handling for IRQ5
jmp DEFAULT 								; No handling for IRQ6
jmp DEFAULT 								; No handling for IRQ7
jmp DEFAULT 								; No handling for Timer 2 Compare
jmp DEFAULT 								; No handling for Timer 2 Overflow
jmp DEFAULT 								; No handling for Timer 1 Capture
jmp DEFAULT 								; No handling for Timer 1 CompareA
jmp DEFAULT 								; No handling for Timer 1 CompareB
jmp DEFAULT 								; No handling for Timer 1 Overflow
jmp DEFAULT 								; No handling for Timer 0 Compare
jmp Timer0 									; Jump to the interrupt handler for Timer 0 overflow 
jmp DEFAULT 								; No handling for all other interrupts

DEFAULT: reti
RESET:
ldi temp, low(RAMEND)
out SPL, temp
ldi temp, high(RAMEND)
out SPH, temp
ldi temp, PORTCDIR 							; columns are outputs, rows are inputs
out DDRC, temp
rcall lcd_init
clr release
clr num_stat
rcall get_num_stat
ldi temp, FAST_PWM
out TCCR0, temp 							; Prescaling value=8�_ 256*8/7.3728 
ldi temp, 1<<TOIE0 							; =278 microseconds 
out TIMSK, temp 							; T/C0 interrupt enable 
sei 										; Enable global interrupt
Clear TempCounter
Clear SecondCounter
ldi temp, 0xFF
mov train_status, temp
rcall get_sta_name
cli
rcall get_time_travel
rcall get_stop_time
rcall print_complete_ini
ldi r24, LCD_DISP_ON						; turn on LCD and no cursor blink
rcall lcd_wait_busy
rcall lcd_write_com 
ldi   r24, 0xFF
out   DDRB, r24         					; DDRB=0xFF
clr count2
clr move_cursor
ldi temp, 2
mov train_status, temp
;update NextEventTimeStamp to 5sec
Clear TempCounter
Clear SecondCounter
ldi yl, low(NextEventSecond)
ldi yh, high(NextEventSecond)
ldi r24, 5
st y+, r24
clr r24
st y, r24
; point next station data pointerto station 1
ldi xl, low(station_names)
ldi xh, high(station_names)
ldi zl, low(time_travel)
ldi zh, high(time_travel)
; Enable external interup 4 & 5
ldi temp, (2 << ISC50) | (2 << ISC40)
out EICRB, temp
in temp, EIMSK
ori temp, (1<<INT5) | (1<<INT4)
out EIMSK, temp
ldi release, 1
sei
jmp main






EXT_INT4_5:
in r24, SREG 
push r24
push temp

clr temp
cp train_status, temp
brne skip_set_stop_request
ldi temp, 1
mov train_status, temp
skip_set_stop_request:

pop temp
pop r24 							; Epilogue starts here 
out SREG, r24 						; Restore all conflict registers from the stack 
reti

Timer0: 
push yh 							; Prologue starts here 
push yl 							; Save all conflict registers in the prologue 
push temp
push r25 
push r24 
in r24, SREG 
push r24 							; Prologue ends here

ldi temp, 3
cp train_status, temp
breq NotSecond

ldi yl, low(TempCounter) 			; Load the address of the temporary 
ldi yh, high(TempCounter) 			; counter 
ld r24, y+ 							; Load the value of the temporary counter 
ld r25, y
adiw r25:r24, 1 					; Increase the temporary counter by one
st y, r25 							; Store the value of the temporary counter 
st -y, r24 
cpi r24, low(3597) 					; Check if (r25:r24)=3597 
ldi temp, high(3597) 				; 3597= 106/278 
cpc r25, temp 
brne NotSecond
rcall check_for_cursor
rcall check_for_event
clr temp 							; One second has passed since last interrupt 
st y+, temp 						; Reset the temporary counter 
st y, temp 
ldi yl, low(SecondCounter) 			; Load the address of the second 
ldi yh, high(SecondCounter) 		; counter 
ld r24, y+ 							; Load the value of the second counter 
ld r25, y
adiw r25:r24, 1 					; Increase the second counter by one
st y, r25 							; Store the value of the second counter 
st -y, r24 
NotSecond:
rcall check_for_blink
pop r24 							; Epilogue starts here 
out SREG, r24 						; Restore all conflict registers from the stack 
pop r24 							; in the epilogue 
pop r25 
pop temp
pop yl 
pop yh 
reti 								; Return from the interrupt


check_for_cursor:
push temp
push r24
ldi temp, 1
cp move_cursor, temp
brne skip_check_curs
clr move_cursor
ldi r24, LCD_CURS_RIG
rcall lcd_wait_busy
rcall lcd_write_com
skip_check_curs:
pop r24
pop temp
ret


check_for_event:
push yh 							; Prologue starts here 
push yl 							; Save all conflict registers in the prologue 
push temp
push r25
push r24

ldi temp, 0xFF
cp train_status, temp				; if we are initializing the train, don't check for any event
breq skip_handle_event



ldi yl, low(NextEventSecond)
ldi yh, high(NextEventSecond)
ld r0, y+
ld r1, y+
ldi yl, low(SecondCounter)
ldi yh, high(SecondCounter)
ld r2, y+
ld r3, y+
cp r2, r0
cpc r3, r1
brlo skip_handle_event

ldi temp, 1
cp train_status, temp
brne go_straight_to_next_station
									; handle event: reach a station  and have to stop
ldi temp, 2							; indicate that the train is stoping at station	train_status = 0
mov train_status, temp			
rcall stop_the_train				; stop the train
sbiw x, 11							; point x to current station name
rcall display_arrive_station		; display arrived station name
adiw x, 11							; point x to next station name again
ldi yl, low(stop_time)				; update next event time stamp
ldi yh, high(stop_time)				; by add stop time to it
ld temp, y
ldi yl, low(NextEventSecond)
ldi yh, high(NextEventSecond)
ld r24, y+
ld r25, y
add r24, temp
clr temp
adc r25, temp
st y, r25
st -y, r24 
rjmp skip_handle_event

; if dont have to stop or already stop then handle this event by:
; update next station on LCD, update next time event
go_straight_to_next_station:
clr train_status					; indicate that the train is going to next station
									; with-out stop at next station: train_status = 0
rcall run_the_train					; re-run the train  
inc count2
cp count2, num_stat
brne skip_restart_cycle
clr count2
ldi xl, low(station_names)
ldi xh, high(station_names)
ldi zl, low(time_travel)
ldi zh, high(time_travel)
skip_restart_cycle:
; display next station name
rcall display_next_station
adiw x, 11							; point station name pointer to next station 
ld temp, z+							; also point time travel pointer to next station
ldi yl, low(SecondCounter)		; update next event time stamp
ldi yh, high(SecondCounter)		; by adding time travel to the next station
ld r24, y+
ld r25, y
add r24, temp
clr temp
adc r25, temp
ldi yl, low(NextEventSecond)
ldi yh, high(NextEventSecond)
st y+, r24
st y, r25
skip_handle_event:
pop r24 							; in the epilogue 
pop r25 
pop temp
pop yl 
pop yh
ret

check_for_blink:
push yh
push yl
push temp
push r25
push r24
ldi temp, 0xFF
cp train_status, temp				; if we are initializing the train, don't check for any event
breq end_blink_handle
ldi temp, 2
cp train_status, temp				; if the train is running don't blink
brlo end_blink_handle
ldi yl, low(BlinkCounter)
ldi yh, high(BlinkCounter)
ld r24, y+
ld r25, y
sbiw r25:r24, 1
st y, r25
st -y, r24
brsh end_blink_handle
ldi r24, low(900)
ldi r25, high(900)
st y+, r24
st y, r25
ldi temp, 1
cp blink_status, temp
brne led_off
ldi temp, LED_ON
out PORTB, temp
clr blink_status
rjmp end_blink_handle
led_off:
ldi temp, LED_OF
out PORTB, temp
ldi temp, 1
mov blink_status, temp

end_blink_handle:
pop r24
pop r25
pop temp
pop yl
pop yh
ret

; Function print to LCD: Arrived at: station name"
str6: .db "Arrived at:     " 
display_arrive_station:
push xh
push xl
push zh
push zl
push r24
push count
ldi r24, LCD_DISP_CLR
rcall lcd_wait_busy
rcall lcd_write_com
ldi zl, low(str6 << 1)
ldi zh, high(str6 << 1)
ldi count, str_len
rcall print_string
ldi r24, LCD_ADDR_SET | LCD_LINE2
rcall lcd_wait_busy
rcall lcd_write_com
display_station_loop:
ld r24, x+
cpi r24, 0
breq end_display_arive
rcall lcd_wait_busy
rcall lcd_write_data
rjmp display_station_loop
end_display_arive:
pop count
pop r24
pop zl
pop zh
pop xl
pop xh
ret

; Function print to LCD: Next station: station name"
str7: .db "Next station:   "
display_next_station:
push xh
push xl
push zh
push zl
push r24
push count
ldi r24, LCD_DISP_CLR
rcall lcd_wait_busy
rcall lcd_write_com
ldi zl, low(str7 << 1)
ldi zh, high(str7 << 1)
ldi count, str_len
rcall print_string
ldi r24, LCD_ADDR_SET | LCD_LINE2
rcall lcd_wait_busy
rcall lcd_write_com
display_loop:
ld r24, x+
cpi r24, 0
breq end_display_loop
rcall lcd_wait_busy
rcall lcd_write_data
rjmp display_loop
end_display_loop:
pop count
pop r24
pop zl
pop zh
pop xl
pop xh
ret

; Set speed of mortor to 0
stop_the_train:
push temp
ldi temp, STOP
out OCR0, temp
pop temp
ret

; Set speed of mortor to 60% duty cycle
run_the_train:
push temp
ldi temp, RUN
out OCR0, temp
pop temp
ret

;Function lcd_write_com: Write a command to the LCD. The data reg stores the value to be written.
lcd_write_com:
push temp
out PORTD, r24   							; set the temp port's value up
clr temp
out PORTA, temp 							; RS = 0, RW = 0 for a command write
nop 										; delay to meet timing (Set up time)
sbi PORTA, LCD_E 							; turn on the enable pin
nop 										; delay to meet timing (Enable pulse width)
nop
nop
cbi PORTA, LCD_E 							; turn off the enable pin
nop 										; delay to meet timing (Enable cycle time)
nop
nop
pop temp
ret

;Function lcd_write_data: Write a character to the LCD. The temp reg stores the value to be written.
lcd_write_data:
push temp
out PORTD, r24	 							; set the data port's value up
ldi temp, 1 << LCD_RS
out PORTA, temp 							; RS = 1, RW = 0 for a data write
nop 										; delay to meet timing (Set up time)
sbi PORTA, LCD_E 							; turn on the enable pin
nop 										; delay to meet timing (Enable pulse width)
nop
nop
cbi PORTA, LCD_E 							; turn off the enable pin
nop 										; delay to meet timing (Enable cycle time)
nop
nop
pop temp
ret

;Function lcd_wait_busy: Read the LCD busy flag until it reads as not busy.
lcd_wait_busy:
push temp
clr temp
out DDRD, temp 								; Make PORTD be an input port for now
out PORTD, temp
ldi temp, 1 << LCD_RW
out PORTA, temp 							; RS = 0, RW = 1 for a command port read
busy_loop:
nop 										; delay to meet timing (Set up time / Enable cycle time)
sbi PORTA, LCD_E 							; turn on the enable pin
nop 										; delay to meet timing (Data delay time)
nop
nop
in temp, PIND 								; read value from LCD
cbi PORTA, LCD_E 							; turn off the enable pin
sbrc temp, LCD_BF 							; if the busy flag is set
rjmp busy_loop 								; repeat command read
clr temp									; else
out PORTA, temp 							; turn off read mode,
ser temp
out DDRD, temp								; make PORTD an output port again
pop temp
ret 										; and return

; Function delay: Pass a number in registers r18:r19 to indicate how many microseconds
; must be delayed. Actual delay will be slightly greater (~1.08us*r18:r19).
; r18:r19 are altered in this function.
; Code is omitted
delay:										; perform r18:r19 * 7 clock note
subi del_lo, 1
sbci del_hi, 0
nop
nop
nop
nop
brne delay
ret

;Function lcd_init Initialisation function for LCD.
lcd_init:
ser temp
out DDRD, temp 					; PORTD, the data port is usually all otuputs
out DDRA, temp 					; PORTA, the control port is always all outputs
ldi del_lo, low(15000)
ldi del_hi, high(15000)
rcall delay 					; delay for > 15ms
; Function set command with N = 1 and F = 0
ldi r24, LCD_FUNC_SET | (1 << LCD_N)
rcall lcd_write_com 			; 1st Function set command with 2 lines and 5*7 font
ldi del_lo, low(4100)
ldi del_hi, high(4100)
rcall delay 					; delay for > 4.1ms
ldi r24, LCD_FUNC_SET | (1 << LCD_N)
rcall lcd_write_com 			; 2nd Function set command with 2 lines and 5*7 font
ldi del_lo, low(100)
ldi del_hi, high(100)
rcall delay 					; delay for > 100us
ldi r24, LCD_FUNC_SET | (1 << LCD_N)
rcall lcd_write_com 			; 3rd Function set command with 2 lines and 5*7 font
rcall lcd_write_com 			; Final Function set command with 2 lines and 5*7 font
rcall lcd_wait_busy 			; Wait until the LCD is ready
ldi r24, LCD_DISP_OFF
rcall lcd_write_com 			; Turn Display off
rcall lcd_wait_busy 			; Wait until the LCD is ready
ldi r24, LCD_DISP_CLR
rcall lcd_write_com 			; Clear Display
rcall lcd_wait_busy 			; Wait until the LCD is ready
; Entry set command with I/D = 1 and S = 0
ldi r24, LCD_ENTRY_SET | (1 << LCD_ID)
rcall lcd_write_com 			; Set Entry mode: Increment = yes and Shift = noF
rcall lcd_wait_busy 			; Wait until the LCD is ready
; Display on command with C = 0 and B = 1
ldi r24, LCD_DISP_ON | (1 << LCD_C)
rcall lcd_write_com 			;Trun Display on with a cursor that doesn't blink
ret

; Function keypad scanning: keep scanning until a key is pressed, then return
; key value to r24
keypad_scan:
push temp
push temp2
push temp3
start_scan:
ldi temp3, INITCOLMASK				; initial column mask
clr col 							; initial column
colloop:
out PORTC, temp3 					; set column to mask value
; (sets column 0 off)
ldi temp, 0xFF 						; implement a delay so the
									; hardware can stabilize
q_delay:
dec temp
brne q_delay

in temp, PINC 						; read PORTC
andi temp, ROWMASK					; read only the row bits
cpi temp, 0xF						; check if any rows are grounded
breq nextcol						; if not go to the next column
ldi temp3, INITROWMASK 				; initialise row check
clr row								; initial row
rowloop:
mov temp2, temp
and temp2, temp3 					; check masked bit
brne skipconv 						; if the result is non-zero,
									; we need to look again
									; if bit is clear, escape scanning
cpi release, 1						; check if the previous button was not 
brne start_scan						; releas , start scan again
clr release							; else go to scan found to convert
jmp scan_found						; 
skipconv:
inc row 							; else move to the next row
lsl temp3 							; shift the mask to the next bit
jmp rowloop
nextcol:
cpi col, 3 							; check if we�re on the last column
brne skip_set_re 					; if so, no buttons were pushed, so set release
ldi release, 1						; and start again.
jmp start_scan
skip_set_re:
sec 								; else shift the column mask:
									; We must set the carry bit
rol temp3 							; and then rotate left by a bit,
									; shifting the carry into
									; bit zero. We need this to make									
									; sure all the rows have
									; pull-up resistors
inc col 							; increment column value
jmp colloop 						; and check the next column
									; convert function converts the row and column given to a
									; binary number and also outputs the value to PORTC.
									; Inputs come from registers row and col and output is in
									; temp.
scan_found:
cpi col, 3 							; if column is 3 we have a letter
breq letters
cpi row, 3 							; if row is 3 we have a symbol or 0
breq symbols
mov temp, row 						; otherwise we have a number (1-9)
lsl temp 							; temp = row * 2
add temp, row 						; temp = row * 3
add temp, col 						; add the column address
									; to get the offset from 1
inc temp 							; add 1. Value of switch is
									; row*3 + col + 1.
ldi r24, '0'
add r24, temp

jmp convert_end
letters:
ldi r24, 0xA
add r24, row 						; increment from 0xA by the row value
jmp convert_end
symbols:
cpi col, 0 							; check if we have a star
breq star
cpi col, 1 							; or if we have zero
breq zero
ldi r24, '#' 						
jmp convert_end
star:
ldi r24, '*' 						
jmp convert_end
zero:
ldi r24, '0' 		
convert_end:
pop temp3
pop temp2
pop temp
ret


; Function that gets number from keypad and display on LCD
; maximum 2 digit then return input value to r24
; no back-space builtin
scan_number:
push temp
push temp2
push count
start_scan_number:
clr temp2
clr count
clr r24
scan_next_digit:
rcall keypad_scan
cpi count, 0
brne skip_clear_LCD
mov temp, r24
ldi r24, LCD_DISP_CLR
rcall lcd_wait_busy
rcall lcd_write_com
mov r24, temp
skip_clear_LCD:
inc count
cpi r24, '#'
breq end_scan_number
cpi count, 3					; if exceed 2 digit, force reenter
brlo skip_re_scan
rcall print_reenter
rjmp start_scan_number
skip_re_scan:			
cpi r24, '0'
brlo scan_next_digit
cpi r24, '9' + 1
brsh scan_next_digit
mov temp, temp2					; temporary return value
lsl temp2						; is store in temp2 reg
lsl temp2						; let temp2*10 + r24
lsl temp2
lsl temp
add temp2, temp
mov temp, r24
subi temp, '0'
add temp2, temp					; print r24 from key_pad scan
rcall lcd_wait_busy
rcall lcd_write_data
rjmp scan_next_digit
end_scan_number:
mov r24, temp2					; copy result to return reg
pop count
pop temp2
pop temp
ret

; Function scan string from keypad, only capital alphabet character 
; and space (key 0),back-space bultin (key *) restrict to 10 char
; finally store string to data memory that pointer x point to
scan_name:
push temp
push temp2
push temp3
push xl
push xh
start_scan_char:
clr temp3
clr temp2
clr count
clr move_cursor
scan_char:
rcall keypad_scan
cpi count, 0
brne skip_clr_LCD
mov temp, r24
ldi r24, LCD_DISP_CLR
rcall lcd_wait_busy
rcall lcd_write_com
mov r24, temp
skip_clr_LCD:
cpi r24, '#'
brne countinue
jmp end_scan_char
countinue:
cpi count, 11
brlo skip_re_scan_char
rcall print_reenter
sbiw x, 10
rjmp start_scan_char
skip_re_scan_char:
cpi r24, '0'
brne skip_space
ldi temp, 1								; if have to move cursor right
cp move_cursor, temp
brne skip_move_cs
ldi r24, LCD_CURS_RIG
rcall lcd_wait_busy
rcall lcd_write_com
skip_move_cs:
ldi r24, ' '
rcall lcd_wait_busy
rcall lcd_write_data
clr move_cursor
jmp store_char
skip_space:
cpi r24, '*'
brne skip_back_space
cpi count, 0							; if start string, back-space has
breq scan_char							; no effect
ldi temp, 1								
cp move_cursor, temp
breq skip_move_curs
ldi r24, LCD_CURS_LEF
rcall lcd_wait_busy
rcall lcd_write_com
skip_move_curs:
clr move_cursor
ldi r24, ' '
rcall lcd_wait_busy
rcall lcd_write_data
ldi r24, LCD_CURS_LEF
rcall lcd_wait_busy
rcall lcd_write_com
sbiw x, 1
dec count
jmp scan_char
skip_back_space:
cpi r24, '2'							; only alow alphabetic character
brlt scan_char
cpi r24, '9' + 1
brge scan_char
cp r24, temp3							; if is the same as last typed key
brne skip_encode						
ldi temp, 1								; and cursor still under the current char
cp move_cursor, temp					; start encode
brne skip_encode
clr move_cursor							; mark dont have to move cursor anymore
sbiw x, 1								; re write data at current position
dec count
inc temp2
cpi temp3, '7'
breq case7
cpi temp3, '8'
breq case8
cpi temp3, '9'
breq case9
cpi temp2, 3
brne convert_key
clr temp2
rjmp convert_key
case7:
cpi temp2, 4
brne convert_key
clr temp2
rjmp convert_key
case8:
cpi temp2, 4
brne convert_key
ldi temp2, 1
rjmp convert_key
case9:
cpi temp2, 5
brne convert_key
ldi temp2, 1
rjmp convert_key

skip_encode:
mov temp3, r24					; update last typed key
clr temp2
cpi temp3, '8'
brlt convert_key
ldi temp2, 1
convert_key:
subi r24, '2'
mov temp, r24
lsl r24							; let r24 = (number typed from key pad - 2) * 3
add r24, temp                   ; and then r24 = r24 + temp2 +'A'
ldi temp, 'A'                   ; temp2 means encoded addition
add r24, temp                   
add r24, temp2

ldi temp, 1						; if have to move cursor right
cp move_cursor, temp
brne skip_move_cursor
mov temp, r24
ldi r24, LCD_CURS_RIG
rcall lcd_wait_busy
rcall lcd_write_com
mov r24, temp
skip_move_cursor:				; print char to cursor position
rcall lcd_wait_busy
rcall lcd_write_data
mov temp, r24
ldi r24, LCD_CURS_LEF			; turn cursor back to last position
rcall lcd_wait_busy				; for further encode
rcall lcd_write_com
mov r24, temp
clr move_cursor					; mark cursor is under last character
inc move_cursor					; (move_cursor = 1) that have to move at next second
Clear TempCounter				; reset tempcpunter to wait for next second event
store_char:
st x+, r24
inc count						; increase number of char
rjmp scan_char					
end_scan_char:
cpi count, 1
brsh skip_re_scan_name
rcall print_reenter
jmp start_scan_char
skip_re_scan_name:

ldi r24, 0						; store end of string
st x+, r24
pop xh
pop xl
pop temp3
pop temp2
pop temp
ret

; Function print string from program memory
; that z points to with count is it's length
print_string:
push r24
push count
print_each_char:
lpm r24, z+                    ; read a character from the string 
rcall lcd_wait_busy
rcall lcd_write_data            ; write the character to the screen
dec count                       ; decrement character counter
brne print_each_char
pop count
pop r24
ret

; Function print number store in r24, maximum 10
print_number:
push temp
cpi r24, 10
brne skip_print_10
ldi r24, '1'
rcall lcd_wait_busy
rcall lcd_write_data
ldi r24, '0'
rcall lcd_wait_busy
rcall lcd_write_data
rjmp end_print_number
skip_print_10:
ldi temp, '0'
add r24, temp
rcall lcd_wait_busy
rcall lcd_write_data
end_print_number:
pop temp
ret

; Function print request: re type the input
str0_1: .db "Invalid! Please "
str0_2: .db "type again      "
print_reenter:
push count
push r24
ldi r24, LCD_DISP_CLR
rcall lcd_wait_busy
rcall lcd_write_com
ldi zl, low(str0_1 << 1)
ldi zh, high(str0_1 << 1)
ldi count, str_len
rcall print_string
ldi r24, LCD_ADDR_SET | LCD_LINE2
rcall lcd_wait_busy
rcall lcd_write_com
ldi zl, low(str0_2 << 1)
ldi zh, high(str0_2 << 1)
rcall print_string
pop r24
pop count
ret

; Function get number of station from keypad and LCD
str1_1: .db "holy            " 
str1_2: .db "of stations pls "
.equ max_num_stat = 10
.equ min_num_stat = 1
get_num_stat:
rcall lcd_wait_busy
ldi r24, LCD_DISP_CLR
rcall lcd_write_com
ldi zl, low(str1_1 << 1)
ldi zh, high(str1_1 << 1)
ldi count, str_len
rcall print_string
rcall lcd_wait_busy
ldi r24, LCD_ADDR_SET | LCD_LINE2
rcall lcd_write_com
ldi zl, low(str1_2 << 1)
ldi zh, high(str1_2 << 1)
rcall print_string
scan_num_stat:
rcall scan_number
ldi temp, max_num_stat + 1
cp r24, temp
brge reenter1
ldi temp, min_num_stat
cp r24, temp
brlo reenter1
jmp skip_reenter1
reenter1:
rcall print_reenter
rjmp scan_num_stat
skip_reenter1:
mov num_stat, r24
ret


; Function get station names from keypad and display on screen
str2_1: .db "Please type name" 
str2_2: .db "of stations "
str2_3: .db ":   "
.equ str_2_2_len = 12
.equ str_2_3_len = 4
.equ max_num_char = 10
get_sta_name:
clr count2
ldi xl, low(station_names)
ldi xh, high(station_names)
loop2:
inc count2
ldi r24, LCD_DISP_CLR
rcall lcd_wait_busy
rcall lcd_write_com
ldi zl, low(str2_1 << 1)
ldi zh, high(str2_1 << 1)
ldi count, str_len
rcall print_string
ldi r24, LCD_ADDR_SET | LCD_LINE2
rcall lcd_wait_busy
rcall lcd_write_com
ldi zl, low(str2_2 << 1)
ldi zh, high(str2_2 << 1)
ldi count, str_2_2_len
rcall print_string
mov r24, count2
rcall print_number
print_last_str:
ldi zl, low(str2_3 << 1)
ldi zh, high(str2_3 << 1)
ldi count, str_2_3_len
rcall print_string
rcall scan_name								; start scan name
adiw x, 11
cp count2, num_stat
brne loop2
ret


;Function get time travel from keypad and display on LCD
str3_1: .db "Time from staton" 
str3_2: .db " to "
str3_3: .db " is:        "
.equ str3_2_len = 4
.equ str3_3_len = 12
.equ max_time_travel = 10
.equ min_time_travel = 1
get_time_travel:
clr count2
ldi xl, low(time_travel)
ldi xh, high(time_travel)
adiw x, 1
loop3:
inc count2
ldi r24, LCD_DISP_CLR
rcall lcd_wait_busy
rcall lcd_write_com
ldi zl, low(str3_1 << 1)
ldi zh, high(str3_1 << 1)
ldi count, str_len
rcall print_string
ldi r24, LCD_ADDR_SET | LCD_LINE2
rcall lcd_wait_busy
rcall lcd_write_com
mov r24, count2
rcall print_number
print_to:
ldi zl, low(str3_2 << 1)
ldi zh, high(str3_2 << 1)
ldi count, str3_2_len
rcall print_string


cp count2, num_stat
brne skip_print_1
ldi r24, 1
skip_print_1:
mov r24, count2
inc r24
rcall print_number

ldi zl, low(str3_3 << 1)
ldi zh, high(str3_3 << 1)
ldi count, str3_3_len
rcall print_string

scan_time_travel:
rcall scan_number
ldi temp, max_time_travel + 1
cp r24, temp
brsh reenter3
ldi temp, min_time_travel
cp r24, temp
brlo reenter3
jmp skip_reenter3
reenter3:
rcall print_reenter
rjmp scan_time_travel
skip_reenter3:
cp count2, num_stat
breq skip_loop3
st x+, r24
jmp loop3
skip_loop3:
ldi xl, low(time_travel)
ldi xh, high(time_travel)
st x, r24
ret



;Function get stop time at any station
str4_1: .db "Stop time at any" 
str4_2: .db "station is:     "
.equ min_stop_time = 2
.equ max_stop_time = 5
get_stop_time:
ldi r24, LCD_DISP_CLR
rcall lcd_wait_busy
rcall lcd_write_com
ldi zl, low(str4_1 << 1)
ldi zh, high(str4_1 << 1)
ldi count, str_len
rcall print_string
ldi r24, LCD_ADDR_SET | LCD_LINE2
rcall lcd_wait_busy
rcall lcd_write_com
ldi zl, low(str4_2 << 1)
ldi zh, high(str4_2 << 1)
rcall print_string
scan_stop_time:
rcall scan_number
ldi temp, max_stop_time + 1
cp r24, temp
brsh reenter4
ldi temp, min_stop_time
cp r24, temp
brlo reenter4
jmp skip_reenter4
reenter4:
rcall print_reenter
rjmp scan_stop_time
skip_reenter4:
ldi xl, low(stop_time)
ldi xh, high(stop_time)
st x, r24
ret

; Function print message complete configuration
str5_1: .db "Complete config "
str5_2: .db "Wait for 5s     "
print_complete_ini:
ldi r24, LCD_DISP_CLR
rcall lcd_wait_busy
rcall lcd_write_com
ldi zl, low(str5_1 << 1)
ldi zh, high(str5_1 << 1)
ldi count, str_len
rcall print_string
rcall lcd_wait_busy
ldi r24, LCD_ADDR_SET | LCD_LINE2
rcall lcd_write_com
ldi zl, low(str5_2 << 1)
ldi zh, high(str5_2 << 1)
rcall print_string
ret

scan_hash_key:
push temp
push r25
push r24
ldi temp, HASHCOL
out PORTC, temp
ldi del_lo, 0xFF 				; implement a delay so the
clr del_hi 						; hardware can stabilize
rcall delay
in temp, PINC
andi temp, ROWMASK
andi temp, HASHROW
brne hash_not_pressed
cpi release, 1
brne end_hash_scan
clr release
hash_not_pressed:
ldi release, 1 
end_hash_scan:
pop r24
pop r25
pop temp
ret

main:
ldi temp, 2
cp train_status, temp
brge main 
rcall keypad_scan
cpi r24, '#'
breq stop_half_way
rjmp main
stop_half_way:
rcall stop_the_train
mov temp2, train_status
ldi temp, 3						; stuck
mov train_status, temp
stop_loop:
rcall keypad_scan
cpi r24, '#'
brne stop_loop
rcall run_the_train
mov train_status, temp2
jmp main
