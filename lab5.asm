.include "m64def.inc"

.def temp 		= r16
.def temp2 		= r17
.def temp3		= r18
.def row 		= r19
.def col 		= r20
.def release	= r21
.def speed		= r22


;keypad set up
.equ portcdir 	= 0xf0
.equ initcolmask= 0xef
.equ initrowmask= 0x01
.equ rowmask 	= 0x0f

;pwm set up
.equ max_speed	= 204
.equ ini_speed	= 51 				; at 20% duty cycle
.equ interval	= 13				; of 5% duty cycle
.equ stop		= 0
.equ fast_pwm	= 0b01101100		; set fast pwm, invert and prescale of 64


.cseg
.org 0
jmp reset

reset:
ldi temp, low(ramend)
out spl, temp
ldi temp, high(ramend)
out sph, temp
ldi temp, portcdir 							; columns are outputs, rows are inputs
out ddrc, temp
ldi temp, fast_pwm 
out tccr0, temp
ldi temp, 0xff
out ddrb, temp
ldi speed, ini_speed
out ocr0, speed
jmp main


; function keypad scanning: keep scanning until a key is pressed, then return
; key value to r24
keypad_scan:
push temp
push temp2
push temp3
start_scan:
ldi temp3, initcolmask				; initial column mask
clr col 							; initial column
colloop:
out portc, temp3 					; set column to mask value
; (sets column 0 off)
ldi temp, 0xff 						; implement a delay so the
									; hardware can stabilize
q_delay:
dec temp
brne q_delay

in temp, pinc 						; read portc
andi temp, rowmask					; read only the row bits
cpi temp, 0xf						; check if any rows are grounded
breq nextcol						; if not go to the next column
ldi temp3, initrowmask 				; initialise row check
clr row								; initial row
rowloop:
mov temp2, temp
and temp2, temp3 					; check masked bit
brne skipconv 						; if the result is non-zero,
									; we need to look again
									; if bit is clear, escape scanning
cpi release, 1						; check if the previous button was not 
brne start_scan						; releas , start scan again
clr release							; else go to scan found to convert
jmp scan_found						; 
skipconv:
inc row 							; else move to the next row
lsl temp3 							; shift the mask to the next bit
jmp rowloop
nextcol:
cpi col, 3 							; check if we�re on the last column
brne skip_set_re 					; if so, no buttons were pushed, so set release
ldi release, 1						; and start again.
jmp start_scan
skip_set_re:
sec 								; else shift the column mask:
									; we must set the carry bit
rol temp3 							; and then rotate left by a bit,
									; shifting the carry into
									; bit zero. we need this to make									
									; sure all the rows have
									; pull-up resistors
inc col 							; increment column value
jmp colloop 						; and check the next column
									; convert function converts the row and column given to a
									; binary number and also outputs the value to portc.
									; inputs come from registers row and col and output is in
									; temp.
scan_found:
cpi col, 3 							; if column is 3 we have a letter
breq letters
cpi row, 3 							; if row is 3 we have a symbol or 0
breq symbols
mov temp, row 						; otherwise we have a number (1-9)
lsl temp 							; temp = row * 2
add temp, row 						; temp = row * 3
add temp, col 						; add the column address
									; to get the offset from 1
inc temp 							; add 1. value of switch is
									; row*3 + col + 1.
ldi r24, '0'
add r24, temp

jmp convert_end
letters:
ldi r24, 0xa
add r24, row 						; increment from 0xa by the row value
jmp convert_end
symbols:
cpi col, 0 							; check if we have a star
breq star
cpi col, 1 							; or if we have zero
breq zero
ldi r24, '#' 						
jmp convert_end
star:
ldi r24, '*' 						
jmp convert_end
zero:
ldi r24, '0' 		

convert_end:
pop temp3
pop temp2
pop temp
ret

main:
out ocr0, speed
rcall keypad_scan
cpi r24, '1'
brne case2
cpi speed, 246
brsh skip_add_speed
ldi temp, interval
add speed, temp
rjmp main


skip_add_speed:
ldi speed, max_speed
rjmp main


case2:
cpi r24, '2'
brne case3
cpi speed, 61
brlo main
subi speed, interval
rjmp main


case3:
cpi r24, '3'
brne case4
ldi speed, stop
rjmp main


case4:
cpi r24, '4'
brne main
ldi speed, ini_speed
rjmp main
