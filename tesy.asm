.include "m64def.inc"
.def temp =r16
.def row =r17
.def col =r18
.def mask =r19
.def temp2 =r20
.equ PORTDDIR = 0xF0
.equ INITCOLMASK = 0xEF
.equ INITROWMASK = 0x01
.equ ROWMASK = 0x0F

.def data =r21

.def num1 = r9
.def num2 = r10
.def num3 = r11

.def lastDigit = r22
.def numLength = r23
.def currentNumber = r24
.def currentTotal = r25
.def currentHash = r26

;LCD protocol control bits
.equ LCD_RS = 3
.equ LCD_RW = 1
.equ LCD_E = 2
;LCD functions
.equ LCD_FUNC_SET = 0b00110000
.equ LCD_DISP_OFF = 0b00001000
.equ LCD_DISP_CLR = 0b00000001
.equ LCD_DISP_ON = 0b00001100
.equ LCD_ENTRY_SET = 0b00000100
.equ LCD_ADDR_SET = 0b10000000
;LCD function bits and constants
.equ LCD_BF = 7
.equ LCD_N = 3
.equ LCD_F = 2
.equ LCD_ID = 1
.equ LCD_S = 0
.equ LCD_C = 1
.equ LCD_B = 0
.equ LCD_LINE1 = 0
.equ LCD_LINE2 = 0x40

rjmp main

;;;;;;;;;;;;;;;;;;; START LCD FUNCTIONS;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;Function lcd_write_com: Write a command to the LCD. The data reg stores the value to be written.
lcd_write_com:
	out PORTC, data ; set the data port's value up
	clr temp
	out PORTA, temp ; RS = 0, RW = 0 for a command write
	nop ; delay to meet timing (Set up time)
	sbi PORTA, LCD_E ; turn on the enable pin
	nop ; delay to meet timing (Enable pulse width)
	nop
	nop
	cbi PORTA, LCD_E ; turn off the enable pin
	nop ; delay to meet timing (Enable cycle time)
	nop
	nop
	ret

;Function lcd_write_data: Write a character to the LCD. The data reg stores the value to be written.
lcd_write_data:
	out PORTC, data ; set the data port's value up
	ldi temp, 1 << LCD_RS
	out PORTA, temp ; RS = 1, RW = 0 for a data write
	nop ; delay to meet timing (Set up time)
	sbi PORTA, LCD_E ; turn on the enable pin
	nop ; delay to meet timing (Enable pulse width)
	nop
	nop
	cbi PORTA, LCD_E ; turn off the enable pin
	nop ; delay to meet timing (Enable cycle time)
	nop
	nop
	ret

;Function lcd_wait_busy: Read the LCD busy flag until it reads as not busy.
lcd_wait_busy:
	clr temp
	out DDRC, temp ; Make PORTC be an input port for now
	out PORTC, temp
	ldi temp, 1 << LCD_RW
	out PORTA, temp ; RS = 0, RW = 1 for a command port read

busy_loop:
	nop ; delay to meet timing (Set up time / Enable cycle time)
	sbi PORTA, LCD_E ; turn on the enable pin
	nop ; delay to meet timing (Data delay time)
	nop
	nop
	in temp, PINC ; read value from LCD
	cbi PORTA, LCD_E ; turn off the enable pin
	sbrc temp, LCD_BF ; if the busy flag is set
	rjmp busy_loop ; repeat command read
	clr temp ; else
	out PORTA, temp ; turn off read mode,
	ser temp
	out DDRC, temp ; make PORTC an output port again
	ret ; and return

;Function lcd_init Initialisation function for LCD.
lcd_init:
	ser temp
	out DDRC, temp ; PORTC, the data port is usually all otuputs
	out DDRA, temp ; PORTA, the control port is always all outputs
	rcall delayFunc ; delay for > 15ms
	; Function set command with N = 1 and F = 0
	ldi data, LCD_FUNC_SET | (1 << LCD_N)
	rcall lcd_write_com ; 1st Function set command with 2 lines and 5*7 font
	rcall delayFunc ; delay for > 4.1ms
	rcall lcd_write_com ; 2nd Function set command with 2 lines and 5*7 font
	rcall delayFunc ; delay for > 100us
	rcall lcd_write_com ; 3rd Function set command with 2 lines and 5*7 font
	rcall lcd_write_com ; Final Function set command with 2 lines and 5*7 font
	rcall lcd_wait_busy ; Wait until the LCD is ready
	ldi data, LCD_DISP_OFF
	rcall lcd_write_com ; Turn Display off
	rcall lcd_wait_busy ; Wait until the LCD is ready
	ldi data, LCD_DISP_CLR
	rcall lcd_write_com ; Clear Display
	rcall lcd_wait_busy ; Wait until the LCD is ready
	; Entry set command with I/D = 1 and S = 0
	ldi data, LCD_ENTRY_SET | (1 << LCD_ID)
	rcall lcd_write_com ; Set Entry mode: Increment = yes and Shift = no
	rcall lcd_wait_busy ; Wait until the LCD is ready
	; Display on command with C = 0 and B = 1
	ldi data, LCD_DISP_ON | (1 << LCD_C)
	rcall lcd_write_com ; Trun Display on with a cursor that doesn't blink
	ret

delayFunc:
    push r18
	push r19
	push r20
	ldi  r18, 2
    ldi  r19, 160
    ldi  r20, 147
	delayLoop :
		dec  r20
		brne delayLoop
		dec  r19
		brne delayLoop
		dec  r18
		brne delayLoop
		nop
	pop r20
	pop r19
	pop r18
	ret
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; END LCD ;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;


;;;;;;;;;;;;;;;;;;;;;;;;;;;;;; START KEYPAD FUNCTIONS;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
; getInput checks for keypad input and returns it in temp register
getInput:
	push mask
	push temp
	push row
	push col
	push temp2

	ldi mask, INITCOLMASK ; initial column mask
	clr col ; initial column
colloop:
	out PORTD, mask ; set column to mask value
	; (sets column 0 off)
	ldi temp, 0xFF 
	
; implement a delay so the hardware can stabilize
delay:
	dec temp
	brne delay
	in temp, PIND ; read PORTD
	andi temp, ROWMASK ; read only the row bits
	cpi temp, 0xF ; check if any rows are grounded
	breq nextcol ; if not go to the next column
	ldi mask, INITROWMASK ; initialise row check
	clr row ; initial row

rowloop:
	mov temp2, temp
	and temp2, mask ; check masked bit
	brne skipconv ; if the result is non-zero,
	; we need to look again
	rjmp convert ; if bit is clear, convert the bitcode
	jmp endGetInput ; and start again

skipconv:
	inc row ; else move to the next row
	lsl mask ; shift the mask to the next bit
	jmp rowloop

nextcol:
	cpi col, 3 ; check if we�re on the last column
	breq endGetInput ; if so, no buttons were pushed,
	; so start again.
	
	sec ; else shift the column mask:
	; We must set the carry bit
	rol mask ; and then rotate left by a bit,
	; shifting the carry into
	; bit zero. We need this to make
	; sure all the rows have
	; pull-up resistors
	inc col ; increment column value
	jmp colloop ; and check the next column
	; convert function converts the row and column given to a
	; binary number and also outputs the value to PORTC.
	; Inputs come from registers row and col and output is in
	; temp.

convert:
	cpi col, 3 ; if column is 3 we have a letter
	breq letters
	cpi row, 3 ; if row is 3 we have a symbol or 0
	breq symbols
	mov temp, row ; otherwise we have a number (1-9)
	lsl temp ; temp = row * 2
	add temp, row ; temp = row * 3
	add temp, col ; add the column address
	; to get the offset from 1
	inc temp ; add 1. Value of switch is
	; row*3 + col + 1.
	jmp convert_end

letters:
	ldi temp, 0xA
	add temp, row ; increment from 0xA by the row value
	jmp convert_end

symbols:
	cpi col, 0 ; check if we have a star
	breq star
	cpi col, 1 ; or if we have zero
	breq zero
	ldi temp, 0xF ; we'll output 0xF for hash
	jmp convert_end

star:
	ldi temp, 0xE ; we'll output 0xE for star
	jmp convert_end

zero:
	clr temp ; set to zero

convert_end:
	mov data, temp
	rjmp endGetInput ; return with our key value via endGetInput

endGetInput:
	pop temp2
	pop col
	pop row
	pop temp
	pop mask

	ret

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;; END KEYPAD FUNCTIONS ;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

main:
	ldi temp, low(RAMEND)			; initialise stack pointer
	out SPL, temp
	ldi temp, high(RAMEND)
	out SPH, temp
	
	; set up keypad
	ldi temp, PORTDDIR ; columns are outputs, rows are inputs
	out DDRD, temp
	ser temp
	out DDRB, temp ; Make PORTB all outputs
	clr temp
	out PORTB, temp ; Turn on all the LEDs

	clr currentTotal
	clr currentNumber
	clr numLength
	clr num1
	clr num2
	clr num3
	clr currentHash
	ser lastDigit

	rcall lcd_init
	
	inputPoll:
		ldi data, 30
		rcall getInput

		cp data, lastDigit
		breq inputPoll
		
		cpi data, 15
		breq addAndWrite

		cpi data, 10
		brge resetLastDigit

		rjmp addToNum

	resetLastDigit:
		mov lastDigit, data
		rjmp inputPoll

	addToNum:
		mov lastDigit, data
		
		rcall delayFunc
		rcall delayFunc
		rcall delayFunc

		cpi numLength, 0
		brne checkLength
		clr currentNumber ; Clear the current number if we are entering a new one

		checkLength:
			cpi numLength, 3
			brge inputPoll ; called max of 3 times
			inc numLength

			ldi temp, 10
			mul currentNumber, temp
			mov currentNumber, r0
			add currentNumber, data

		rjmp inputPoll
	
	addAndWrite:
		inc currentHash

		;Clear off the display and reset cursor
		rcall lcd_wait_busy	
		ldi data, LCD_DISP_CLR
		rcall lcd_write_com ; Clear Display
		rcall lcd_wait_busy
		ldi data, LCD_ADDR_SET | LCD_LINE1
		rcall lcd_write_com


		mov lastDigit, data

		add currentTotal, currentNumber

		; Get the currentTotal, and split it into 3 digits
		clr num1
		clr num2
		clr num3
		ldi numLength, 1

		mov data, currentTotal
		rcall writeInteger
		
		rcall lcd_wait_busy
		ldi data, LCD_ADDR_SET | LCD_LINE2
		rcall lcd_write_com

		mov data, currentHash
		rcall writeInteger

		clr numLength

		rcall delayFunc
		rcall delayFunc
		rcall delayFunc

		rjmp inputPoll				

writeInteger:
		clr num1
		clr num2
		clr num3
		clr numLength
		getNumOne:
			cpi data, 100
			brlo getNumTwo
			ldi numLength, 3
			inc num1
			subi data, 100
			rjmp getNumOne
		getNumTwo:
			cpi data, 10
			brlo getNumThree
			inc num2
			subi data, 10
			cpi numLength, 3
			breq getNumTwo
			ldi numLength, 2
			rjmp getNumTwo
		getNumThree:
			cpi data, 0
			breq writeNumbers
			inc num3
			dec data
			rjmp getNumThree

		writeNumbers:
			cpi numLength, 2
			brlt WriteNumThree
			breq WriteNumTwo

		writeNumOne:
			ldi temp, '0'
			add num1, temp
			mov data, num1
			rcall lcd_wait_busy
			rcall lcd_write_data
		writeNumTwo:
			ldi temp, '0'
			add num2, temp
			mov data, num2
			rcall lcd_wait_busy
			rcall lcd_write_data
		writeNumThree:
			ldi temp, '0'
			add num3, temp
			mov data, num3
			rcall lcd_wait_busy
			rcall lcd_write_data

		ret

end:
	rjmp end
