; Determines if the car is on the top line or bottom line
; and calls the corresponding display function
display_car:
	rcall lcd_send_cursor_home

	; Clear the 
	rcall lcd_send_cursor_home
	cp is_car_top, one
	breq display_car_top

	rcall display_car_bottom
	ret

	; Shifts the cursor down to the bottom line before calling the
	; display function
	display_car_bottom:
		rcall lcd_line_two
		rcall display_car_top

		ret

	; Displays the car at its current position
	display_car_top:
		ldi temp2, 7
		rcall lcd_shift_cursor_right
		
		mov temp2, distance
		rcall lcd_shift_cursor_right

		ldi data, CAR
		rcall lcd_write_data
		rcall lcd_wait_busy

		ret

; displays the current level
display_level:
	rcall lcd_send_cursor_home
	ldi temp2, 2
	rcall lcd_shift_cursor_right

	ldi data, ASCII_ZERO
	add data, level
	rcall lcd_write_data
	rcall lcd_wait_busy

	ret

;displays current lives
display_lives:
	rcall lcd_send_cursor_home
	ldi temp2, 6
	rcall lcd_shift_cursor_right

	ldi data, ASCII_ZERO
	add data, lives
	rcall lcd_write_data
	rcall lcd_wait_busy
	ret

; displays current score
; calls function to convert all three digits
display_score:
	out PORTC, score
	rcall lcd_send_cursor_home
	rcall lcd_line_two

	ldi temp2, 3
	rcall lcd_shift_cursor_right

	mov digit3, score
	rcall convert_number

	mov data, digit1
	rcall lcd_write_data
	rcall lcd_wait_busy

	mov data, digit2
	rcall lcd_write_data
	rcall lcd_wait_busy

	mov data, digit3
	rcall lcd_write_data
	rcall lcd_wait_busy

	clr digit1
	clr digit2
	clr digit3

	ret

; checks each bit to see if an obstacle is there
; then prints them out
display_obstacles:
	rcall lcd_send_cursor_home
	rcall display_top_obstacles
	rcall display_bottom_obstacles
	ret

	display_top_obstacles:
		ldi temp2, 7
		rcall lcd_shift_cursor_right
		rcall check_row_top
		ret

	display_bottom_obstacles:
		rcall lcd_line_two
		ldi temp2, 7
		rcall lcd_shift_cursor_right
		rcall check_row_bottom
		ret

	check_row_top:
		check_one_top:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, top_row
			andi temp, MASK_ONE
			breq check_two_top
			rcall print_obstacle

		check_two_top:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, top_row
			andi temp, MASK_TWO
			breq check_three_top
			rcall print_obstacle

		check_three_top:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, top_row
			andi temp, MASK_THREE
			breq check_four_top
			rcall print_obstacle

		check_four_top:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, top_row
			andi temp, MASK_FOUR
			breq check_five_top
			rcall print_obstacle

		check_five_top:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, top_row
			andi temp, MASK_FIVE
			breq check_six_top
			rcall print_obstacle

		check_six_top:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, top_row
			andi temp, MASK_SIX
			breq check_seven_top
			rcall print_obstacle

		check_seven_top:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, top_row
			andi temp, MASK_SEVEN
			breq check_eight_top
			rcall print_obstacle

		check_eight_top:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, top_row
			andi temp, MASK_EIGHT
			breq check_done
			rcall print_obstacle

		check_done:
			ret

	check_row_bottom:
		check_one_bottom:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, bottom_row
			andi temp, MASK_ONE
			breq check_two_bottom
			rcall print_obstacle

		check_two_bottom:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, bottom_row
			andi temp, MASK_TWO
			breq check_three_bottom
			rcall print_obstacle

		check_three_bottom:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, bottom_row
			andi temp, MASK_THREE
			breq check_four_bottom
			rcall print_obstacle

		check_four_bottom:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, bottom_row
			andi temp, MASK_FOUR
			breq check_five_bottom
			rcall print_obstacle

		check_five_bottom:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, bottom_row
			andi temp, MASK_FIVE
			breq check_six_bottom
			rcall print_obstacle

		check_six_bottom:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, bottom_row
			andi temp, MASK_SIX
			breq check_seven_bottom
			rcall print_obstacle

		check_seven_bottom:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, bottom_row
			andi temp, MASK_SEVEN
			breq check_eight_bottom
			rcall print_obstacle

		check_eight_bottom:
			ldi temp2, 1
			rcall lcd_shift_cursor_right

			mov temp, bottom_row
			andi temp, MASK_EIGHT
			breq check_done
			rcall print_obstacle
			jmp check_done

	print_obstacle:
		ldi data, OBSTACLE
		rcall lcd_write_data
		rcall lcd_wait_busy
		
		ldi temp2, 1
		rcall lcd_shift_cursor_left

		ret
