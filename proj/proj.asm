; Connection prerequisites
;         Mot -> PE7 (as labelled on the board, in code it is actually PE2)
;     R0 - R3 -> PB0 - PB3
;     C0 - C3 -> PB4 - PB7
;     D0 - D3 -> PD0 - PD3
;     D4 - D7 -> PD4 - PD7
;     BE - RS -> PA0 - PA3
; LED0 - LED3 -> PC0 - PC3
; LED4 - LED7 -> PC4 - PC7

.include "m64def.inc"

; State
.def      score = r2 	 ; the current score
.def multiplier = r3     ; score multiplier 
					     ; actual score = multiplier*0xFF + score
.def      level = r4 	 ; the current level
.def      lives = r5 	 ; how many lives remain

; Need these constants in registers because immediate instructions
; do not work for r0 -> r15
.def    zero = r6
.def     one = r7
.def   eight = r8
.def     ten = r9
.def hundred = r10

; Location
.def is_car_top = r11	; Is the car in the top row?
.def   distance = r12 	; How far is the car from pipe column on the lcd?
						; is compared to 'eight' register to determine if
						; end has been reached

.def    top_row = r13	; Map of the top row, 1s represent obstacles
.def bottom_row = r14	; As above for bottom row

.def  power_ups = r15	; Map of power-up locations
						; 4 most significant bits represent top row
						; 4 least significant bits represent bottom row

; temporary data manipulation registers
.def  temp = r16
.def temp2 = r17
.def  data = r18

; used for keypad polling
.def  row = r19
.def  col = r20
.def mask = r21

; used for displaying the current score on the lcd
.def digit1 = r22
.def digit2 = r23
.def digit3 = r24

; used to count the time left until advancing all obstacles on screen
; value related to current level
.def advCounter = r25

; lcd delay registers to wait for lcd to update
.def del_lo = r26
.def del_hi = r27

; counters to count the seconds until the next level
.def counter  = r28
.def counter2 = r29
.def counter3 = r30
.def secondCounter = r31

; Constants
.equ MAX_LIVES = 3
.equ       CAR = 'C'
.equ  OBSTACLE = 'O'
.equ  POWER_UP = 'P'

; Keyboard port constants
.equ    PORTBDIR = 0xF0
.equ INITCOLMASK = 0xEF
.equ INITROWMASK = 0x01
.equ     ROWMASK = 0x0F

; Obstacle location masks
.equ MASK_EIGHT = 0b00000001
.equ MASK_SEVEN = 0b00000010
.equ MASK_SIX   = 0b00000100
.equ MASK_FIVE  = 0b00001000
.equ MASK_FOUR  = 0b00010000
.equ MASK_THREE = 0b00100000
.equ MASK_TWO   = 0b01000000
.equ MASK_ONE   = 0b10000000

; Motor speed constant
.equ RPS_70 = 220

.cseg
	jmp RESET
	jmp Default ; IRQ0 Handler
	jmp Default ; IRQ1 Handler
	jmp Default ; IRQ2 Handler
	jmp Default ; IRQ3 Handler
	jmp Default ; IRQ4 Handler
	jmp Default ; IRQ5 Handler
	jmp Default ; IRQ6 Handler
	jmp Default ; IRQ7 Handler
	jmp Default ; Timer2 Compare Handler
	jmp Timer2  ; Timer2 Overflow Handler, handles advancement of obstacles
	jmp Default ; Timer1 Capture Handler
	jmp Default ; Timer1 CompareA Handler
	jmp Default ; Timer1 CompareB Handler
	jmp Default ; Timer1 Overflow Handler
	jmp Default ; Timer0 Compare Handler
	jmp Timer0  ; Timer0 Overflow Handler, handles level time counting and motor operation
	Default: reti

RESET:
	clr r0
	clr r1

	; State variables
	clr score
	clr multiplier
	clr level
	clr lives

	; Constants
	clr zero
	clr one
	clr eight
	clr ten
	clr hundred

	; Variables related to car position
	clr is_car_top
	clr distance

	; Variables related to items and obstacles
	clr top_row
	clr bottom_row
	clr power_ups

	; Temporary variables
	clr temp
	clr temp2
	clr data

	; Keypad variables
	clr row
	clr col
	clr mask

	; Display variables
	clr digit1
	clr digit2
	clr digit3

	clr counter

	; Initialise stack
	ldi temp, low(RAMEND)
	out SPL, temp
	ldi temp, high(RAMEND)
	out SPH, temp

	; Set up Port B
	ldi temp, PORTBDIR 		; columns are outputs, rows are inputs
	out DDRB, temp

	; Set up Port C
	ldi temp, 0xFF
	out DDRC, temp

	; Set up Port E
	ldi temp, 0xFF
	out DDRE, temp

	; Initialise registers
	ldi temp, 0
	mov zero, temp
	mov score, temp
	
	ldi temp, 1
	mov one, temp
	mov level, temp
	mov is_car_top, temp
	mov distance, temp

	ldi temp, MAX_LIVES
	mov lives, temp

	ldi temp, 8
	mov eight, temp

	ldi temp, 10
	mov ten, temp

	ldi temp, 100
	mov hundred, temp

	sei

	rcall lcd_init
	
	rcall random_init	

	rcall timer_set_level_advance

	jmp main

; LCD, display, timing and random number
; functions are included in other files
.include "lcd_functions.asm"
.include "display_functions.asm"
.include "time_functions.asm"
.include "randomNumbers.asm"
.include "endgame_displays.asm"

; If the user has reached level 10, win game
go_to_win_screen:
	jmp winner

; main keeps scanning the keypad to find which key is pressed.
main:
	ldi mask, INITCOLMASK 		; initial column mask
	clr col 					; initial column

	cp level, ten
	breq go_to_win_screen		; if you reach level ten you win the game

	rcall collision_detector

colloop:
	out PORTB, mask 			; set column to mask value
								; (sets column 0 off)
	ldi temp, 0xFF 				; implement a delay so the
								; hardware can stabilize

delay2:
	dec temp
	brne delay2
	in temp, PINB 				; read PORTB
	andi temp, ROWMASK 			; read only the row bits
	cpi temp, 0xF				; check if any rows are grounded
	breq nextcol 				; if not go to the next column
	ldi mask, INITROWMASK 		; initialise row check
	clr row 					; initial row

rowloop:
	mov temp2, temp
	and temp2, mask				; check masked bit
	brne skipconv 				; if the result is non-zero,
								; we need to look again

keyup_delay:
	in temp2, PINB
	andi temp2, ROWMASK			; wait for key to be up before proceeding	
	cpi temp2, 0xF
	brne keyup_delay

	rcall convert_keypress		; if bit is clear, convert the bitcode
	jmp main 					; and start again

skipconv:
	inc row 					; else move to the next row
	lsl mask 					; shift the mask to the next bit
	jmp rowloop

nextcol:
	cpi col, 3 					; check if we’re on the last column
	breq invalid_key			; if so, no buttons were pushed,
								; so start again.
	sec 						; else shift the column mask:
								; We must set the carry bit
	rol mask 					; and then rotate left by a bit,
								; shifting the carry into
								; bit zero. We need this to make
								; sure all the rows have
								; pull-up resistors
	inc col 					; increment column value
	jmp colloop 				; and check the next column
								; convert function converts the row and column given to a
								; binary number and also outputs the value to PORTC.
								; Inputs come from registers row and col and output is in
								; temp.

invalid_key:
	jmp main 					; do nothing

convert_keypress:
	rcall InitRandom			; seed number generator on keypresses

	cpi row, 3 					; if row is 3 we have a symbol or 0
	breq main

	mov temp, row 				; otherwise we have a number (1-9)
	lsl temp 					; temp = row * 2
	add temp, row 				; temp = row * 3
	add temp, col 				; add the column address
								; to get the offset from 1
	inc temp 					; add 1. Value of switch is
								; row*3 + col + 1.

	; check which key has been pressed
	cpi temp, 2
	breq move_up

	cpi temp, 4
	breq move_left

	cpi temp, 5
	breq skip_level

	cpi temp, 6
	breq move_right

	cpi temp, 8
	breq move_down

	; It wasn't one of the control keys so go back to main
	ret

; Move the car up if it's not already in the top row
move_up:
	cp is_car_top, zero
	breq move_car_up
	ret

	move_car_up:
		add is_car_top, one
		rcall lcd_display_game
		ret

	; Move the car down if it's not already in the bottom row
	move_down:
		cp is_car_top, one
		breq move_car_down
		ret

		move_car_down:
			sub is_car_top, one
			rcall lcd_display_game
			ret

	; Move the car left if it isn't already at end of display
	move_left:
		cp distance, one
		brne move_car_left
		ret

		move_car_left:
			sub distance, one
			rcall lcd_display_game
			ret

	; Move the car right if it isn't already at end of display
	move_right:
		cp distance, eight
		brne move_car_right
		ret

		move_car_right:
			add distance, one
			rcall lcd_display_game
			ret

	skip_level:
		; clear all obstacles
		inc level
		cp level, ten
		breq go_to_winner

		mov top_row, zero
		mov bottom_row, zero

		; move car back to origin
		mov is_car_top, one
		mov distance, one

		; otherwise, start the level again
		rcall lcd_display_game
		rcall timer_set_level_advance

		ret

		go_to_winner:
			jmp winner

; Called when timer ticks
; os_advance is called when obstacles are to move to the left (when the obstacle
;            advancement timer ticks over). It calculates new obstacles etc.
; os_top is called when we intend to add another obstacle to the top row
; os_bottom is called when we intend to add another obstacle to the bottom row
; Either can be used when no obstacles are being added 
; (i.e. when the carry flag is cleared)

obstacle_shift_advance:	
	; check we didn't just create an obstacle
	; if we did, don't make a new obstacle
	mov temp, top_row
	andi temp, 0x01
	brne no_new_obstacle

	mov temp, bottom_row
	andi temp, 0x01
	brne no_new_obstacle

	; no recently created obstacle,
	; check if we can make a new one
	rcall GetRandom
	
	; 25% chance of top obstacle
	cpi temp, 64
	brlo new_obstacle_top
	; 25% chance of bottom obstacle
	cpi temp, 128
	brlo new_obstacle_bot
	

	no_new_obstacle:
		clc
		jmp obstacle_shift_top
	new_obstacle_top:
		sec
		jmp obstacle_shift_top
	new_obstacle_bot:
		sec
		jmp obstacle_shift_bottom

; move all obstacles, if carry flag is set a 
; new obstacle is generated
obstacle_shift_top:
	rol top_row
	rcall update_score
	clc
	rol bottom_row
	rcall update_score
	jmp obstacle_shift_end

obstacle_shift_bottom:
	rol bottom_row
	rcall update_score
	clc
	rol top_row
	rcall update_score

obstacle_shift_end:
	rcall lcd_display_game
	ret

; if an obstacle is at the left of the screen
; it is removed and put in the carry flag.
; checks flag and increment score if it is set
update_score:
	brcs increment_score
	ret

	increment_score:
		add score, level
		brcs increment_multiplier
		ret

		;if we have overflow, incrememnt the multiplier
		increment_multiplier:
			inc score
			inc multiplier
			ret

; Converts a number into ascii format 
convert_number: 				
	; takes the number in register digit3 and converts it into 3 individual 
	; ascii characters that get stored in registers digit1, digit2 and digit3
	ldi digit1,'0'-1
	; count how many hundreds in the number in ascii format
	hundreds: 
		inc digit1 
		subi digit3, 100        
		brcc hundreds
	ldi digit2, 10+'0'
	; count how many tens in the number in ascii format
	tens: 	
		dec digit2
		subi digit3, -10 
		brcs tens
	; convert the units part into ascii
	subi digit3, -'0' 

	mov temp, multiplier
	tst multiplier
	brne multiply

	clr temp
	ret

; Applies the multiplier
multiply:
	dec temp

	digit3_add:
		subi digit3, -5
		cpi digit3, ':'
		brge digit2_carry

	digit2_add:
		subi digit2, -5
		cpi digit2, ':'
		brge digit1_carry

	digit1_add:
		subi digit1, -2
		cpi digit1, ':'
		brge nineninenine

	tst temp
	brne multiply

	ret

	digit2_carry:
		subi digit3, 10
		inc digit2
		cpi digit2, ':'
		brge digit1_carry
		rjmp digit2_add

	digit1_carry:
		subi digit2, 10
		inc digit1
		cpi digit1, ':'
		brge nineninenine
		rjmp digit1_add

	nineninenine:
		ldi digit1, '9'
		ldi digit2, '9'
		ldi digit3, '9'
		ret

; Collision detection
; checks if there is an obstacle where the car is
; if there is, go to life_lost
; if there isn't return from the subroutine
collision_detector:
	mov temp, distance
	tst is_car_top
	breq collision_check_bottom

	collision_check_top:
		cpi temp, 8
		breq collision_check_top_eight
		cpi temp, 7
		breq collision_check_top_seven
		cpi temp, 6
		breq collision_check_top_six
		cpi temp, 5
		breq collision_check_top_five
		cpi temp, 4
		breq collision_check_top_four
		cpi temp, 3
		breq collision_check_top_three
		cpi temp, 2
		breq collision_check_top_two
		cpi temp, 1
		breq collision_check_top_one

		collision_check_top_eight:
			sbrc top_row, 0
			jmp life_lost
			ret

		collision_check_top_seven:
			sbrc top_row, 1
			jmp life_lost
			ret

		collision_check_top_six:
			sbrc top_row, 2
			jmp life_lost
			ret

		collision_check_top_five:
			sbrc top_row, 3
			jmp life_lost
			ret

		collision_check_top_four:
			sbrc top_row, 4
			jmp life_lost
			ret

		collision_check_top_three:
			sbrc top_row, 5
			jmp life_lost
			ret

		collision_check_top_two:
			sbrc top_row, 6
			jmp life_lost
			ret

		collision_check_top_one:
			sbrc top_row, 7
			jmp life_lost
			ret

	collision_check_bottom:
		cpi temp, 8
		breq collision_check_bottom_eight
		cpi temp, 7
		breq collision_check_bottom_seven
		cpi temp, 6
		breq collision_check_bottom_six
		cpi temp, 5
		breq collision_check_bottom_five
		cpi temp, 4
		breq collision_check_bottom_four
		cpi temp, 3
		breq collision_check_bottom_three
		cpi temp, 2
		breq collision_check_bottom_two
		cpi temp, 1
		breq collision_check_bottom_one

		collision_check_bottom_eight:
			sbrc bottom_row, 0
			jmp life_lost
			ret

		collision_check_bottom_seven:
			sbrc bottom_row, 1
			jmp life_lost
			ret

		collision_check_bottom_six:
			sbrc bottom_row, 2
			jmp life_lost
			ret

		collision_check_bottom_five:
			sbrc bottom_row, 3
			jmp life_lost
			ret

		collision_check_bottom_four:
			sbrc bottom_row, 4
			jmp life_lost
			ret

		collision_check_bottom_three:
			sbrc bottom_row, 5
			jmp life_lost
			ret

		collision_check_bottom_two:
			sbrc bottom_row, 6
			jmp life_lost
			ret

		collision_check_bottom_one:
			sbrc bottom_row, 7
			jmp life_lost
			ret


; Called when the user has struck an obstacle
life_lost:
	dec lives
	
	ldi temp, MASK_SIX
	out PORTE, temp

	; turn on the motor for two seconds to 'vibrate'
	rcall timer_set_vibrate	

	; game over if you ran out of lives
	tst lives
	breq go_to_gameover

	; clear all obstacles
	mov top_row, zero
	mov bottom_row, zero

	; move car back to origin
	mov is_car_top, one
	mov distance, one

	; otherwise, start the level again
	rcall lcd_display_game
	rcall timer_set_level_advance

	jmp main

go_to_gameover:
	jmp gameover
