; Sample Random Number Generator code
; Author: Jorgen Peddersen
; edited to use functions only
; simply call InitRandom to seed and GetRandom to 
; put a random number in r16

.equ RAND_A = 214013
.equ RAND_C = 2531011

.dseg
.org 0x100
RAND: .byte 4

.cseg

random_init:
	ldi r16, 1 << CS10 ;Start timer
	out TCCR1B, r16
	rcall InitRandom
	ret

InitRandom:
push r16 ; save conflict register

in r16, TCNT1L ; Create random seed from time of timer 1
sts RAND,r16
sts RAND+2,r16
in r16,TCNT1H
sts RAND+1, r16
sts RAND+3, r16

pop r16 ; restore conflict register
ret

GetRandom:
push r0 ; save conflict registers
push r1
push r17
push r18
push r19
push r20
push r21
push r22

clr r22 ; remains zero throughout

ldi r16, low(RAND_C) ; set original value to be equal to C
ldi r17, BYTE2(RAND_C)
ldi r18, BYTE3(RAND_C)
ldi r19, BYTE4(RAND_C)

; calculate A*X + C where X is previous random number.  A is 3 bytes.
lds r20, RAND
ldi r21, low(RAND_A)
mul r20, r21 ; low byte of X * low byte of A
add r16, r0
adc r17, r1
adc r18, r22

ldi r21, byte2(RAND_A)
mul r20, r21  ; low byte of X * middle byte of A
add r17, r0
adc r18, r1
adc r19, r22

ldi r21, byte3(RAND_A)
mul r20, r21  ; low byte of X * high byte of A
add r18, r0
adc r19, r1

lds r20, RAND+1
ldi r21, low(RAND_A)
mul r20, r21  ; byte 2 of X * low byte of A
add r17, r0
adc r18, r1
adc r19, r22

ldi r21, byte2(RAND_A)
mul r20, r21  ; byte 2 of X * middle byte of A
add r18, r0
adc r19, r1

ldi r21, byte3(RAND_A)
mul r20, r21  ; byte 2 of X * high byte of A
add r19, r0

lds r20, RAND+2
ldi r21, low(RAND_A)
mul r20, r21  ; byte 3 of X * low byte of A
add r18, r0
adc r19, r1

ldi r21, byte2(RAND_A)
mul r20, r21  ; byte 2 of X * middle byte of A
add r19, r0

lds r20, RAND+3
ldi r21, low(RAND_A)	
mul r20, r21  ; byte 3 of X * low byte of A
add r19, r0

sts RAND, r16 ; store random number
sts RAND+1, r17
sts RAND+2, r18
sts RAND+3, r19

mov r16, r19  ; prepare result (bits 30-23 of random number X)
lsl r18
rol r16

pop r22 ; restore conflict registers
pop r21 
pop r20
pop r19
pop r18
pop r17
pop r1
pop r0
ret
