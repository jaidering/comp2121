.set PRESCALAR256  = 0b00000110 		;set prescalar to 256 for TCCR0


timer_set_level_advance:
	; enable the Timer0 OVF interrupt for the counting of seconds
	; 30 seconds for each level
	ldi temp, 0b00000010     
	out TCCR0, temp          ; Prescaling value=8  ;256*8/7.3728( Frequency of the clock 7.3728MHz, for the overflow it should go for 256 times)
	ldi temp, 1<<TOIE0       ; =278 microseconds
	out TIMSK, temp          ; T/C0 interrupt enable
	
	; enable the Timer2 OVF intterup for the advancement of the obstacles
	ldi temp, 0b00000100     ; 
	out TCCR2, temp          ; Prescaling value=256
						     ; =278 microseconds
	
	ldi temp, 0b01000001	 ; enable Timer0 and 2 interrupts
	out TIMSK, temp	

	sei                      ; Enable global interrupt

	clr counter
	clr counter2
	clr counter3

	ret

	
timer_set_vibrate:

	;enable the Timer0 OVF interrupt for the stopping of motor vibration
	ldi temp, PRESCALAR256
	out TCCR0, temp 		; set prescaling value to 256
	ldi temp, 1<<TOIE0
	out TIMSK, temp 		; Timer0 overflow interrupt enable	

	;turn on motor
	ser temp
	out DDRE, temp
	ldi temp, 0b00000100
	out PORTE, temp
	
	clr secondCounter
	clr counter

	sei
	; wait until timer goes off before doing
	; ANYTHING else
	motor_loop:
		cpi counter, 224
		breq motor_loop_end
		jmp motor_loop

	motor_loop_end:
		ret

Timer0:

	push temp
	in temp, SREG
	push temp                 ; Prologue ends.

	in temp, TCCR0
	cpi temp, PRESCALAR256
	breq motor_Timer0		  ; If we are in motor checking mode

	; otherwise count the seconds to next level:

	; sample code to count to a second from lecture materials
	/**** a counter for 3597 is needed to get one second-- Three counters are used in this example **************/                                          
	                         ; 3597  (1 interrupt 278microseconds therefore 3597 interrupts needed for 1 sec)
	cpi counter, 97          ; counting for 97
	brne notsecond
 
	cpi counter2, 35         ; counting for 35
	brne secondloop          ; jumping into count 100 

	
	outmot: ldi counter,0    ; clearing the counter values after counting 3597 interrupts which gives us one second
	        ldi counter2,0
	        ldi counter3,0
	        
			; DO THINGS HERE
			inc secondCounter
			cpi secondCounter, 30
			brne exit

			; 30 seconds reached, go to next level
			clr secondCounter
			inc level

			mov is_car_top, one
			mov distance, one
			mov top_row, zero
			mov bottom_row, zero

			rcall lcd_display_game

	        rjmp exit        ; go to exit

	notsecond: 
		inc counter   ; if it is not a second, increment the counter
	    rjmp exit

	secondloop: 
		inc counter3 ; counting 100 for every 35 times := 35*100 := 3500
	    cpi counter3,100 
	    brne exit
		inc counter2
		ldi counter3,0  
		                
	exit: 
		pop temp                  ; Epilogue starts;
		out SREG, temp            ; Restore all conflict registers from the stack.
		pop temp
		reti                     ; Return from the interrupt.

	; motor timer interrupt fired
	motor_Timer0:
		inc counter
		cpi counter, 224
	
		; turn off the motor if we reached 2 seconds
		breq motor_off
		
		rjmp exit
	
		motor_off:
			;turn off motor
			ser temp
			out DDRE, temp
			ldi temp, 0b00000000
			out PORTE, temp

			rjmp exit

Timer2: 
	push temp
	in temp, SREG
	push temp                 ; Prologue ends.

	; calculate the current level speed
	push temp2
	ldi temp, 112	 ; lvl 1, 1 second
	mov temp2, level
	
	cpi temp2, 1
	breq dec_done
	subi temp, 11    ; lvl 2, -10%
	dec temp2
	cpi temp2, 1
	breq dec_done
	subi temp, 11    ; lvl 3, -10%
	dec temp2
	cpi temp2, 1
	breq dec_done
	subi temp, 11    ; lvl 4, -10%
	dec temp2
	cpi temp2, 1
	breq dec_done
	subi temp, 11    ; lvl 5, -10%
	dec temp2
	cpi temp2, 1
	breq dec_done
	subi temp, 12    ; lvl 6, -10% (plus decimals lost from above)
	dec temp2
	cpi temp2, 1
	breq dec_done
	subi temp, 28    ; lvl 7, -25%
	dec temp2
	cpi temp2, 1
	breq dec_done
	subi temp, 16    ; lvl 8, -15%
	dec temp2

	dec_loop:
		cpi temp2, 1
		breq dec_done
		lsr temp     ; lvl 9, halve for each level
		dec temp2
		jmp dec_loop
	
	dec_done:
		pop temp2	

	cp advCounter, temp
	brne notsecond2

	
	outmot2: ldi advCounter,0    ; clearing the counter values after counting number of interrupts hit, 
								 ; which gives us one second
	       
			; DO THINGS HERE
			rcall obstacle_shift_advance
	        rjmp advExit        ; go to advExit

	notsecond2: 
		inc advCounter   ; if it is not a second, increment the counter
	    rjmp advExit
		                
	advExit: 
		pop temp                  ; Epilogue starts;
		out SREG, temp            ; Restore all conflict registers from the stack.
		pop temp
		reti                     ; Return from the interrupt.
