; Called when the user has run out of cars
gameover:
	rcall lcd_wait_busy

	; Clear the display
	ldi data, LCD_DISP_CLR 
	rcall lcd_write_com
	rcall lcd_wait_busy

	ldi data, 'G'
	rcall lcd_write_data
	rcall lcd_wait_busy
	ldi data, 'A'
	rcall lcd_write_data
	rcall lcd_wait_busy
	ldi data, 'M'
	rcall lcd_write_data 
	rcall lcd_wait_busy
	ldi data, 'E'
	rcall lcd_write_data
	rcall lcd_wait_busy
	ldi data, ' '
	rcall lcd_write_data
	rcall lcd_wait_busy

	ldi data, 'O'
	rcall lcd_write_data
	rcall lcd_wait_busy
	ldi data, 'V'
	rcall lcd_write_data 
	rcall lcd_wait_busy
	ldi data, 'E'
	rcall lcd_write_data
	rcall lcd_wait_busy
	ldi data, 'R'
	rcall lcd_write_data
	rcall lcd_wait_busy

	rcall lcd_line_two

	mov digit3, score
	rcall convert_number

	mov data, digit1
	rcall lcd_write_data
	rcall lcd_wait_busy

	mov data, digit2
	rcall lcd_write_data
	rcall lcd_wait_busy

	mov data, digit3
	rcall lcd_write_data
	rcall lcd_wait_busy

	cli

	gameoverloop:
		jmp gameoverloop

; Called when player finishes level 9
winner:
	rcall lcd_wait_busy
	; Clear the display
	ldi data, LCD_DISP_CLR 
	rcall lcd_write_com
	rcall lcd_wait_busy

	ldi data, 'Y'
	rcall lcd_write_data
	rcall lcd_wait_busy
	ldi data, 'O'
	rcall lcd_write_data
	rcall lcd_wait_busy
	ldi data, 'U'
	rcall lcd_write_data 
	rcall lcd_wait_busy

	ldi data, ' '
	rcall lcd_write_data
	rcall lcd_wait_busy

	ldi data, 'W'
	rcall lcd_write_data
	rcall lcd_wait_busy
	ldi data, 'I'
	rcall lcd_write_data 
	rcall lcd_wait_busy
	ldi data, 'N'
	rcall lcd_write_data
	rcall lcd_wait_busy
	ldi data, '!'
	rcall lcd_write_data
	rcall lcd_wait_busy

	rcall lcd_line_two

	mov digit3, score
	rcall convert_number

	mov data, digit1
	rcall lcd_write_data
	rcall lcd_wait_busy

	mov data, digit2
	rcall lcd_write_data
	rcall lcd_wait_busy

	mov data, digit3
	rcall lcd_write_data
	rcall lcd_wait_busy

	cli

	winloop:
		jmp winloop
