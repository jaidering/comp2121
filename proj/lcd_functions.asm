; LCD boilerplate functions

; LCD protocol control bits
.equ LCD_RS = 3
.equ LCD_RW = 1
.equ LCD_E  = 2

; LCD functions
.equ LCD_FUNC_SET  = 0b00110000
.equ LCD_DISP_OFF  = 0b00001000
.equ LCD_DISP_CLR  = 0b00000001
.equ LCD_DISP_ON   = 0b00001100
.equ LCD_ENTRY_SET = 0b00000100
.equ LCD_ADDR_SET  = 0b10000000

.equ LCD_CURSOR_OFF         = 0b00001100
.equ LCD_CURSOR_SHIFT_RIGHT = 0b00010100
.equ LCD_CURSOR_SHIFT_LEFT  = 0b00010000
.equ LCD_CURSOR_HOME        = 0b00000010

; LCD function bits and constants
.equ LCD_BF = 7
.equ LCD_N  = 3
.equ LCD_F  = 2
.equ LCD_ID = 1
.equ LCD_S  = 0
.equ LCD_C  = 1
.equ LCD_B  = 0

.equ LCD_LINE1 = 0
.equ LCD_LINE2 = 0x40

; For display
.equ ASCII_ZERO = '0'

; Function lcd_init Initialisation function for LCD.
lcd_init:
	ser temp
	out DDRD, temp 				; PORTD, the data port is usually all outputs
	out DDRA, temp 				; PORTA, the control port is always all outputs

	ldi del_lo, low(15000)
	ldi del_hi, high(15000)
	rcall delay 				; delay for > 15ms

	; Function set command with N = 1 and F = 0
	ldi data, LCD_FUNC_SET | (1 << LCD_N)
	rcall lcd_write_com 		; 1st Function set command with 2 lines and 5*7 font

	ldi del_lo, low(4100)
	ldi del_hi, high(4100)
	rcall delay 				; delay for > 4.1ms
	rcall lcd_write_com 		; 2nd Function set command with 2 lines and 5*7 font

	ldi del_lo, low(100)
	ldi del_hi, high(100)
	rcall delay 				; delay for > 100us
	rcall lcd_write_com 		; 3rd Function set command with 2 lines and 5*7 font
	rcall lcd_write_com 		; Final Function set command with 2 lines and 5*7 font
	rcall lcd_wait_busy 		; Wait until the LCD is ready

	ldi data, LCD_DISP_OFF
	rcall lcd_write_com 		; Turn Display off
	rcall lcd_wait_busy 		; Wait until the LCD is ready

	ldi data, LCD_DISP_CLR
	rcall lcd_write_com 		; Clear Display
	rcall lcd_wait_busy 		; Wait until the LCD is ready

	; Entry set command with I/D = 1 and S = 0
	ldi data, LCD_ENTRY_SET | (1 << LCD_ID)
	rcall lcd_write_com 		; Set Entry mode: Increment = yes and Shift = no
	rcall lcd_wait_busy 		; Wait until the LCD is ready

	; Display on command with C = 0 and B = 1
	ldi data, LCD_DISP_ON | (1 << LCD_C)
	rcall lcd_write_com 		; Turn Display on with a cursor that doesn't blink
	rcall lcd_wait_busy
	
	rcall lcd_turn_off_cursor
	rcall lcd_display_game

	ret

; Function lcd_write_com: Write a command to the LCD. The data register stores the value to be written.
lcd_write_com:
	out PORTD, data 			; set the data port's value up
	clr temp
	out PORTA, temp 			; RS = 0, RW = 0 for a command write
	nop 						; delay to meet timing (Set up time)
	sbi PORTA, LCD_E 			; turn on the enable pin
	nop 						; delay to meet timing (Enable pulse width)
	nop
	nop
	cbi PORTA, LCD_E 			; turn off the enable pin
	nop 						; delay to meet timing (Enable cycle time)
	nop
	nop
	ret

;Function lcd_write_data: Write a character to the LCD. The data reg stores the value to be written.
lcd_write_data:
	out PORTD, data 			; set the data port's value up
	ldi temp, 1 << LCD_RS
	out PORTA, temp 			; RS = 1, RW = 0 for a data write
	nop 						; delay to meet timing (Set up time)
	sbi PORTA, LCD_E 			; turn on the enable pin
	nop 						; delay to meet timing (Enable pulse width)
	nop
	nop
	cbi PORTA, LCD_E 			; turn off the enable pin
	nop 						; delay to meet timing (Enable cycle time)
	nop
	nop
	ret

; Function lcd_wait_busy: Read the LCD busy flag until it reads as not busy.
lcd_wait_busy:
	clr temp
	out DDRD, temp 				; Make PORTD be an input port for now
	out PORTD, temp
	ldi temp, 1 << LCD_RW
	out PORTA, temp 			; RS = 0, RW = 1 for a command port read
	busy_loop:
		nop 					; delay to meet timing (Set up time / Enable cycle time)
		sbi PORTA, LCD_E 		; turn on the enable pin
		nop 					; delay to meet timing (Data delay time)
		nop
		nop
		in temp, PIND 			; read value from LCD
		cbi PORTA, LCD_E 		; turn off the enable pin
		sbrc temp, LCD_BF 		; if the busy flag is set
		rjmp busy_loop 			; repeat command read
		clr temp 				; else
		out PORTA, temp 		; turn off read mode,
		ser temp
		out DDRD, temp 			; make PORTD an output port again
		ret 					; and return

; Function delay: Pass a number in registers del_lo:del_hi to indicate how many microseconds
; must be delayed. Actual delay will be slightly greater (~1.08us*del_lo:del_hi).
; del_lo:del_hi are altered in this function.
delay:
	loop: 
		subi del_lo, 1
		sbci del_hi, 0
		nop
		nop
		nop
		nop
		brne loop
		ret

; Turns off the cursor
lcd_turn_off_cursor:
	ldi data, LCD_CURSOR_OFF 
	rcall lcd_write_com 		
	rcall lcd_wait_busy
	ret

; Sends the cursor back to its starting position
lcd_send_cursor_home:
	ldi data, LCD_CURSOR_HOME
	rcall lcd_write_com 		
	rcall lcd_wait_busy
	ret

; Shifts the cursor right
; In temp2, store the number of times we want to shift
; DO NOT CALL THIS FUNCTION UNLESS TEMP2 CONTAINS A POSITIVE NUMBER
lcd_shift_cursor_right:
	ldi data, LCD_CURSOR_SHIFT_RIGHT
	rcall lcd_write_com 		
	rcall lcd_wait_busy

	dec temp2
	brne lcd_shift_cursor_right

	ret

; Shifts the cursor left
lcd_shift_cursor_left:

	ldi data, LCD_CURSOR_SHIFT_LEFT
	rcall lcd_write_com 		
	rcall lcd_wait_busy

	dec temp2
	brne lcd_shift_cursor_left

	ret

; Moves the cursor to the start of line 2
lcd_line_two:
	ldi data, LCD_ADDR_SET | LCD_LINE2 
	rcall lcd_write_com
	rcall lcd_wait_busy
	ret

; function to display all game stuff on screen
lcd_display_game:
	rcall lcd_wait_busy

	; Clear the display
	ldi data, LCD_DISP_CLR 
	rcall lcd_write_com
	rcall lcd_wait_busy

	; Level number field (TITLE ONLY)
	ldi data, 'L'
	rcall lcd_write_data
	rcall lcd_wait_busy

	ldi data, ':'
	rcall lcd_write_data
	rcall lcd_wait_busy

	; Number of lives field (TITLE ONLY)
	ldi temp2, 2
	rcall lcd_shift_cursor_right
	clr temp2

	ldi data, CAR
	rcall lcd_write_data 		; write the ascii digit to the screen
	rcall lcd_wait_busy

	ldi data, ':'
	rcall lcd_write_data 		; write the ascii digit to the screen
	rcall lcd_wait_busy

	ldi temp2, 1
	rcall lcd_shift_cursor_right

	ldi data, '|'
	rcall lcd_write_data 		; write the ascii digit to the screen
	rcall lcd_wait_busy

	rcall lcd_line_two 
	
	; Score field (TITLE ONLY)
	ldi data, 'S'
	rcall lcd_write_data 		; write the ascii digit to the screen
	rcall lcd_wait_busy

	ldi data, ':'
	rcall lcd_write_data 		; write the ascii digit to the screen
	rcall lcd_wait_busy

	ldi temp2, 5
	rcall lcd_shift_cursor_right

	ldi data, '|'
	rcall lcd_write_data 		; write the ascii digit to the screen
	rcall lcd_wait_busy

	rcall display_level
	rcall display_lives
	rcall display_score
	rcall display_car
	rcall display_obstacles

	ret
